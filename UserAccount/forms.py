from .models import *
from django.contrib.auth.models import User
from django import forms

class UserLogin(forms.Form):
    username = forms.CharField(max_length = 15,widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(widget = forms.PasswordInput(attrs = {'placeholder':'Password'}))
    class Meta:
        model = User
        fields = ('username','password',)
