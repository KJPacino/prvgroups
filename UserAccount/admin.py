from django.contrib import admin
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.admin import UserAdmin
from .models import *

class UserProfileInline(admin.StackedInline):

    model = UserProfile

class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline,]

admin.site.unregister(User)
admin.site.register(User,UserProfileAdmin)


# Register your models here.
