# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('UserAccount', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='access',
            field=models.CharField(max_length=1, choices=[('O', 'Owner'), ('A', 'Attendant'), ('M', 'Manager'), ('S', 'Admin')]),
        ),
    ]
