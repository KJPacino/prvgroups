# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('location', models.CharField(max_length=1, choices=[('M', 'Main'), ('P', 'Petrol Bunk'), ('T', 'Theater/Marriage Hall')])),
                ('access', models.CharField(max_length=1, choices=[('O', 'Owner'), ('A', 'Attendant'), ('M', 'Manager')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
