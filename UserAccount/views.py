from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
# from datetime import datetime
from django.contrib.auth.decorators import login_required
# from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from CashFlow.models import *
from datetime import date
from cinema.models import *

# today = date.today()

def Userlogin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username= username , password = password)
        if user is not None:
            if user.is_active:
                u = UserProfile.objects.get(user=user)
                if u.access == 'S':
                    login(request,user)
                    return redirect('/admin')
                if u.access == 'O':
                    login(request,user)
                    return redirect('mainpage')
                if u.location == 'P':
                    login(request,user)
                    return redirect('CashFlow/Dashboard')
                if u.location == 'T':
                    login(request,user)
                    return redirect('CollectionReport')
            else:
                return HttpResponse('Account disabled')
        else:
            return HttpResponse('Invalid Username/Password')
    else:
        user = UserLogin()
        return render(request, 'login1.html',{'user':user})

def MainPage(request):
    #checking to see if data refreshes everyday
    today = date.today()

    try:
        credit_card = Credit_Card_Payments.objects.filter(Date = today)
        total_credit_card_payments = 0
        for i in credit_card:
            total_credit_card_payments += i.Amount
    except:
        credit_card = None
        total_credit_card_payments = None

    try:
        tot_revenue = EOD_Total_balances_revenue.objects.filter(Date =  today)
        for i in tot_revenue:
            tot_revenue = i.Total_balances
    except:
        tot_revenue = None


    try:
        Expenses = Expenditure_details.objects.filter(Date = today)
        total_expenditure = 0
        for i in Expenses:
            total_expenditure += i.Amount
    except:
        Expenses = None
        total_expenditure =  None

    try:
        Credit_sales = Credit_details.objects.filter(Date = today)
        total_Credit_sales = 0
        for i in Credit_sales:
            total_Credit_sales += i.Credit_Amt
    except:
        Credit_sales = None
        total_Credit_sales = None

    try:
        eod_cash =  EOD_Cash_account.objects.filter(Date = today)
        for i in eod_cash:
            eod_cash = i.Closing_Cash_Balance
    except:
        eod_cash = None

#cinema

    bills = ShowEntry.objects.filter(date=timezone.now())

    noon = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon")
    matinee = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee")
    firstshow = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show")
    secondshow = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show")

    noon_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    matinee_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    firstshow_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    secondshow_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)

    noon_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    matinee_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    firstshow_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    secondshow_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)

    bills_sum = ShowEntry.objects.filter(date=timezone.now()).aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    day_expense = DayExpense.objects.filter(date=timezone.now()).aggregate(Sum('total')).get('total__sum', 0.00)
    bills_tax = ShowEntry.objects.filter(date=timezone.now()).aggregate(Sum('total_tax')).get('total_tax__sum', 0.00)

    try:
        total_expense = bills_tax + day_expense
    except:
        total_expense = 0

    try:
        net_profit = bills_sum - total_expense
    except:
        net_profit = 0


    distributor_share = CollectionShare.objects.filter(date=timezone.now()).aggregate(Sum('share')).get('share__sum', 0.00)
    try:
        exhi_share = net_profit - distributor_share
    except:
        exhi_share = 0

    return render(request,'basesummary.html',{
    'total_credit_card_payments':total_credit_card_payments,
    'tot_revenue': tot_revenue,
    'total_expenditure': total_expenditure,
    'total_Credit_sales':total_Credit_sales,
    'eod_cash':eod_cash,
    'noon_gross': noon_gross,
    'matinee_gross': matinee_gross,
    'firstshow_gross': firstshow_gross,
    'secondshow_gross': secondshow_gross,
    'bills': bills,
    'noon': noon,
    'matinee': matinee,
    'firstshow': firstshow,
    'secondshow': secondshow,
    'bills_sum': bills_sum,
    'bills_tax': bills_tax,
    'day_expense': day_expense,
    'total_expense': total_expense,
    'net_profit': net_profit,
    'exhi_share': exhi_share,
    'distributor_share': distributor_share,
    'today' : today,

    })
