from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class UserProfile(models.Model):
    ACCESS =(
    ('O','Owner'),
    ('A','Attendant'),
    ('M','Manager'),
    ('S','Admin'),
    )

    LOCATION = (
    ('M','Main'),
    ('P','Petrol Bunk'),
    ('T','Theater/Marriage Hall'),
    )

    user = models.OneToOneField(User)
    location = models.CharField(max_length = 1, choices = LOCATION)
    access = models.CharField(max_length = 1,choices = ACCESS)
