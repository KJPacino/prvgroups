from django.contrib import admin
from .models import *

class ShowEntryAdmin(admin.ModelAdmin):
    list_display = ['show', 'rate', 'tax', 'total_ticket', 'gross_sales', 'total_tax']

admin.site.register(Show)
admin.site.register(Movie)
admin.site.register(CollectionShare)
admin.site.register(ShowDetail)
admin.site.register(ShowEntry, ShowEntryAdmin)
admin.site.register(Invoice)
admin.site.register(DayExpense)
