from django.db import models
from django.utils import timezone
from decimal import Decimal
from django.db.models import Sum

class Show(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Movie(models.Model):
    name = models.CharField(max_length=255)
    distributor = models.CharField(max_length=255)
    date = models.DateField(null=True, auto_now_add=True)

    def __str__(self):
        return u"%s (%s)" % (self.name, self.distributor)

class CollectionShare(models.Model):
    movie_distributor = models.ForeignKey(Movie)
    share = models.DecimalField(blank=True, max_digits=20, decimal_places=2, default=Decimal('0.00'))
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return u"%s %s" % (self.movie_distributor, self.share)

class ShowDetail(models.Model):
    show = models.ForeignKey(Show, null=True)
    time = models.TimeField(blank=True, null=True)
    movie = models.ForeignKey(Movie, null=True)
    date = models.DateField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return str(self.show)

class ShowEntry(models.Model):
    SEAT_TYPE = (
        ('RE', 'RESERVED'),
        ('BA', 'BALCONY'),
    )
    show = models.ForeignKey(Show, null=True)
    time = models.TimeField(blank=True, null=True)
    movie = models.ForeignKey(Movie, null=True)
    date = models.DateField(auto_now_add=True, auto_now=False)
    seat = models.CharField(max_length=2, choices=SEAT_TYPE)
    ticket1 = models.IntegerField()
    ticket2 = models.IntegerField()
    rate = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    tax = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    date = models.DateField(auto_now_add=True)
    '''
    CALCULATE & SAVE THE BELOW
    '''
    total_ticket = models.IntegerField(default=0, editable=False)
    gross_sales = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    total_tax = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))

    def save(self, *args, **kwargs):
        # calculate before saving.
        self.total_ticket = self.calculate_ticket()
        self.gross_sales = self.calculate_gross_sales()
        self.total_tax = self.calculate_tax()
        super(ShowEntry, self).save(*args, **kwargs)

    def calculate_gross_sales(self):
        try:
            total_ticket = self.total_ticket
            rate = self.rate
            return total_ticket * rate
        except KeyError:
            return 0

    def calculate_tax(self):
        try:
            total_ticket = self.total_ticket
            tax = self.tax
            return total_ticket * tax
        except KeyError:
            return 0

    def calculate_ticket(self):
        """ Calculate a numeric value for the model instance. """
        try:
            ticket1 = self.ticket1
            ticket2 = self.ticket2
            return ticket2 - ticket1
        except KeyError:
            # Value_a or value_b is not in the VALUES dictionary.
            # Do something to handle this exception.
            # Just returning the value 0 will avoid crashes, but could
            # also hide some underlying problem with your data.
            return 0

    def __str__(self):
        return str(self.show)

class DayExpense(models.Model):
    kfcc = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    rep_bata = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    inr = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    total = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    date = models.DateField(auto_now_add=True, null=True)

    def save(self, *args, **kwargs):
        # calculate before saving.
        self.total = self.calculate_total_expenses()
        super(DayExpense, self).save(*args, **kwargs)

    def calculate_total_expenses(self):
        try:
            kfcc = self.kfcc
            rep_bata = self.rep_bata
            inr = self.inr
            return kfcc + rep_bata + inr
        except KeyError:
            return 0

    def __str__(self):
        return str(self.total)

class Invoice(models.Model):
    event = models.CharField(max_length=155)
    booking_date = models.DateField(auto_now_add=True)
    event_date = models.DateField(null=True)
    name = models.CharField(max_length=155)
    address = models.CharField(max_length=255)
    place = models.CharField(max_length=80)
    phone = models.CharField(max_length=15)
    AC_CHOICES = (
    ('AC', 'AC'),
    ('NA', 'NA'),
    )
    PAY_TYPE_CHOICES = (
    ('CA', 'Cash'),
    ('CH', 'CHEQUE'),
    )
    RELIGION_CHOICES = (
    ('HI', 'Hindu'),
    ('MU', 'Muslim'),
    ('CH', 'Christian'),
    )
    luxury = models.CharField(max_length=2, choices=AC_CHOICES, default='AC')
    payment_type = models.CharField(max_length=2, choices=PAY_TYPE_CHOICES, default='CA')
    catering = models.BooleanField(default=None)
    religion = models.CharField(max_length=2, choices=RELIGION_CHOICES, default='HI')
    careof = models.CharField(max_length=155)
    amount = models.IntegerField(blank=True, null=True)
    luxury_tax = models.IntegerField(blank=True, null=True)
    total = models.IntegerField(blank=True, null=True)
    advance = models.IntegerField(blank=True, null=True)
    balance = models.IntegerField(blank=True, null=True)

    def save(self, *args, **kwargs):
        # calculate before saving.
        self.total = self.calc_total()
        self.balance = self.calc_balance()
        super(Invoice, self).save(*args, **kwargs)

    def calc_total(self):
        try:
            amount = self.amount
            luxury_tax = self.luxury_tax
            return (amount + luxury_tax)
        except KeyError:
            return 0

    def calc_balance(self):
        try:
            total = self.total
            advance = self.advance
            return (total - advance)
        except KeyError:
            return 0

    def __str__(self):
        return self.event
