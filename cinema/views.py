from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from datetime import date
from django.db.models import Sum
from django.forms.models import formset_factory

from .models import *
from .forms import *

def CollectionReport(request):

    bills = ShowEntry.objects.filter(date=timezone.now())

    noon = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon")
    matinee = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee")
    firstshow = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show")
    secondshow = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show")

    noon_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    matinee_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    firstshow_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)
    secondshow_gross = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show").aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00)

    noon_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Noon").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    matinee_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Matinee").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    firstshow_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="First Show").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)
    secondshow_tax = ShowEntry.objects.filter(date=timezone.now()).filter(show__name="Second Show").aggregate(Sum('total_tax')).get('gross_sales__sum', 0.00)

    bills_sum = ShowEntry.objects.filter(date=timezone.now()).aggregate(Sum('gross_sales')).get('gross_sales__sum', 0.00) or 0
    day_expense = DayExpense.objects.filter(date=timezone.now()).aggregate(Sum('total')).get('total__sum', 0.00) or 0
    bills_tax = ShowEntry.objects.filter(date=timezone.now()).aggregate(Sum('total_tax')).get('total_tax__sum', 0.00) or 0
    total_expense = bills_tax + day_expense
    net_profit = bills_sum - total_expense
    distributor_share = CollectionShare.objects.filter(date=timezone.now()).aggregate(Sum('share')).get('share__sum', 0.00) or 0
    exhi_share = net_profit - distributor_share

    context = {
        'noon_gross': noon_gross,
        'matinee_gross': matinee_gross,
        'firstshow_gross': firstshow_gross,
        'secondshow_gross': secondshow_gross,
        'bills': bills,
        'noon': noon,
        'matinee': matinee,
        'firstshow': firstshow,
        'secondshow': secondshow,
        'bills_sum': bills_sum,
        'bills_tax': bills_tax,
        'day_expense': day_expense,
        'total_expense': total_expense,
        'net_profit': net_profit,
        'exhi_share': exhi_share,
        'distributor_share': distributor_share,
    }
    return render(request, 'cinema/collection-report.html', context)

def addCollection(request):
    if request.method == 'POST':
        form = ShowEntryForm(request.POST);
        if form.is_valid():
            form.save()
            return redirect('CollectionReport')
    else:
        form = ShowEntryForm()
    return render(request, 'cinema/home-shows-events.html', {'form': form})

def addDistributorShare(request):
    if request.method == 'POST':
        form = CollectionShareForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('CollectionReport')
    else:
        form = CollectionShareForm()
        bills = CollectionShare.objects.all().order_by('-date')
    return render(request, 'cinema/add-distributor-share.html', {'form': form, 'bills': bills})

def addDayExp(request):
    if request.method == 'POST':
        form = DayExpenseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('addDayExp')
    else:
        form = DayExpenseForm()
        bills = DayExpense.objects.all()
    return render(request, 'cinema/add_day_expense.html', {'form': form, 'bills': bills})

def addMovie(request):
    if request.method == 'POST':
        form = addMovieForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('addMovie')
    else:
        form = addMovieForm()
    movies = Movie.objects.all().order_by('-date')
    return render(request, 'cinema/add-movie.html', {'form': form, 'movies': movies})

def allInvoice(request):
    bills = Invoice.objects.all()
    return render(request, 'cinema/invoice.html', {'bills': bills})

def AddInvoice(request):
    if request.method == 'POST':
        form = InvoiceForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('allInvoice')
    else:
        form = InvoiceForm()
    return render(request, 'cinema/add-invoice.html', {'form': form})

def summaryview(request):
    if request.method == 'POST':
        datesum = DateSummary(request.POST)
        if datesum.is_valid():
            specified_date = datesum.cleaned_data['Date']

            cinema_bills = ShowEntry.objects.filter(date = specified_date)
            invoice_bills =  Invoice.objects.filter(booking_date = specified_date)

            return render(request,'cinema/cinema-summary.html',{
            'cinema_bills':cinema_bills,
            'invoice_bills':invoice_bills,
            })
    else:
        datesum = DateSummary()

        return render(request,'cinema/cinema-summary.html',{'datesum':datesum})

