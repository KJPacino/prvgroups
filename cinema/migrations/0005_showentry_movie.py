# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0004_auto_20150731_1630'),
    ]

    operations = [
        migrations.AddField(
            model_name='showentry',
            name='movie',
            field=models.ForeignKey(to='cinema.Movie', null=True),
        ),
    ]
