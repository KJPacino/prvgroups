# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CollectionShare',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('share', models.IntegerField(max_length=15)),
                ('date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('distributor', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Show',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ShowDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('time', models.TimeField(null=True, blank=True)),
                ('date', models.DateField(auto_now_add=True)),
                ('movie', models.ForeignKey(to='cinema.Movie')),
                ('show', models.ForeignKey(to='cinema.Show')),
            ],
        ),
        migrations.CreateModel(
            name='ShowEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('seat', models.CharField(max_length=2, choices=[('RE', 'RESERVED'), ('BA', 'BALCONY')])),
                ('ticket1', models.IntegerField()),
                ('ticket2', models.IntegerField()),
                ('rate', models.DecimalField(decimal_places=2, null=True, max_digits=7, blank=True)),
                ('tax', models.DecimalField(decimal_places=2, null=True, max_digits=7, blank=True)),
                ('date', models.DateField(auto_now_add=True)),
                ('total_ticket', models.IntegerField(editable=False, default=0)),
                ('gross_sales', models.DecimalField(editable=False, decimal_places=2, default=0, max_digits=7)),
                ('total_tax', models.DecimalField(editable=False, decimal_places=2, default=0, max_digits=7)),
                ('show', models.ForeignKey(to='cinema.ShowDetail')),
            ],
        ),
        migrations.AddField(
            model_name='collectionshare',
            name='movie_distributor',
            field=models.ForeignKey(to='cinema.Movie'),
        ),
    ]
