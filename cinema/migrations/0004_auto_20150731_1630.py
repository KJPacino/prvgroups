# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0003_invoice'),
    ]

    operations = [
        migrations.AddField(
            model_name='showentry',
            name='time',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='showdetail',
            name='movie',
            field=models.ForeignKey(to='cinema.Movie', null=True),
        ),
        migrations.AlterField(
            model_name='showdetail',
            name='show',
            field=models.ForeignKey(to='cinema.Show', null=True),
        ),
        migrations.AlterField(
            model_name='showentry',
            name='show',
            field=models.ForeignKey(to='cinema.Show', null=True),
        ),
    ]
