# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0002_auto_20150731_1346'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('event', models.CharField(max_length=155)),
                ('booking_date', models.DateField(auto_now_add=True)),
                ('event_date', models.DateField(null=True)),
                ('name', models.CharField(max_length=155)),
                ('address', models.CharField(max_length=255)),
                ('place', models.CharField(max_length=80)),
                ('phone', models.CharField(max_length=15)),
                ('luxury', models.CharField(max_length=2, default='AC', choices=[('AC', 'AC'), ('NA', 'NA')])),
                ('payment_type', models.CharField(max_length=2, default='CA', choices=[('CA', 'Cash'), ('CH', 'CHEQUE')])),
                ('catering', models.BooleanField(default=None)),
                ('religion', models.CharField(max_length=2, default='HI', choices=[('HI', 'Hindu'), ('MU', 'Muslim'), ('CH', 'Christian')])),
                ('careof', models.CharField(max_length=155)),
                ('amount', models.IntegerField(null=True, blank=True)),
                ('luxury_tax', models.IntegerField(null=True, blank=True)),
                ('total', models.IntegerField(null=True, blank=True)),
                ('advance', models.IntegerField(null=True, blank=True)),
                ('balance', models.IntegerField(null=True, blank=True)),
            ],
        ),
    ]
