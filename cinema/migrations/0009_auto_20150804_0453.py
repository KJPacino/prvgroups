# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0008_auto_20150731_1725'),
    ]

    operations = [
        migrations.CreateModel(
            name='DayExpense',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('kfcc', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20)),
                ('rep_bata', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20)),
                ('inr', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20)),
                ('total', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20)),
                ('date', models.DateField(null=True, auto_now_add=True)),
            ],
        ),
        migrations.AlterField(
            model_name='collectionshare',
            name='share',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20),
        ),
        migrations.AlterField(
            model_name='showentry',
            name='gross_sales',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20),
        ),
        migrations.AlterField(
            model_name='showentry',
            name='rate',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20),
        ),
        migrations.AlterField(
            model_name='showentry',
            name='tax',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20),
        ),
        migrations.AlterField(
            model_name='showentry',
            name='total_tax',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=20),
        ),
    ]
