from django import forms

from .models import ShowDetail, ShowEntry, Invoice, CollectionShare, Movie, DayExpense

class ShowDetailForm(forms.ModelForm):
    class Meta:
        model = ShowDetail
        fields = ('show', 'time', 'movie',)

class ShowEntryForm(forms.ModelForm):
    class Meta:
        model = ShowEntry
        fields = ('show', 'time', 'movie', 'seat', 'ticket1', 'ticket2', 'rate', 'tax',)

class InvoiceForm(forms.ModelForm):
    event_date = forms.DateField(widget=forms.TextInput(attrs={'class': 'datepicker'}))
    class Meta:
        model = Invoice
        exclude = ['total', 'balance',]

class CollectionShareForm(forms.ModelForm):
    class Meta:
        model = CollectionShare
        fields = ('movie_distributor', 'share',)

class addMovieForm(forms.ModelForm):
    class Meta:
        model = Movie
        fields = ('name', 'distributor',)

class DayExpenseForm(forms.ModelForm):
    class Meta:
        model = DayExpense
        fields = ('kfcc', 'rep_bata', 'inr')

class DateSummary(forms.Form):
    Date = forms.DateField(widget = forms.TextInput(attrs={'class':'datepicker'}))

