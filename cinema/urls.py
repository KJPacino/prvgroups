from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^collection/report/$', views.CollectionReport, name="CollectionReport"),
    url(r'^collection/add/$', views.addCollection, name="addCollection"),
    url(r'^collection/addShare/$', views.addDistributorShare, name="addShare"),
    url(r'^collection/addDayExp/$', views.addDayExp, name="addDayExp"),
    url(r'^addMovie/$', views.addMovie, name="addMovie"),
    url(r'^invoice/add/$', views.AddInvoice, name="AddInvoice"),
    url(r'^invoice/$', views.allInvoice, name="allInvoice"),
    url(r'^cinema-summary$',views.summaryview, name = 'cinema_summary'),
]
