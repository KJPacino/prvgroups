from django.db import models
from django.contrib.auth.models import User

# class UserProfile(models.Model):
#     ACCESS =(
#     ('O','Owner'),
#     ('A','Attendant'),
#     ('M','Manager'),
#     )
#
#     LOCATION = (
#     ('P','Petrol Bunk'),
#     ('T','Theater/Marriage Hall'),
#     )
#
#     user = models.OneToOneField(User)
#     location = models.CharField(max_length = 1, choices = LOCATION)
#     access = models.CharField(max_length = 1,choices = ACCESS)


class DailySales(models.Model):
    PUMP_NAMES = (
    ('MS-1','MS-1'),
    ('MS-2','MS-2'),
    ('MS-3','MS-3'),
    ('MS-4','MS-4'),
    ('HSD-1','HSD-1'),
    ('HSD-2','HSD-2'),
    ('HSD-3','HSD-3'),
    ('HSD-4','HSD-4'),
    ('HSD-5','HSD-5'),
    )
    Pump_name = models.CharField(max_length = 5, choices = PUMP_NAMES, unique_for_date = 'Date')
    Opening_reading = models.IntegerField()
    Closing_reading = models.IntegerField()
    Gross_sales = models.IntegerField(null = True)
    Date = models.DateField(auto_now_add= True)

    def Calc_gross(self):
        self.Gross_sales = self.Closing_reading - self.Opening_reading


    def __str__(self):
        return '%s, %s' %(self.Pump_name,self.Gross_sales)

class DailyNetSales(models.Model):
    Date = models.DateField(auto_now_add = True)
    Petrol_net_sales = models.IntegerField()
    Diesel_net_sales = models.IntegerField()

class DailyRates(models.Model):
    Date = models.DateField(auto_now_add = True)
    Petrol_rate = models.DecimalField(max_digits=10, decimal_places=2)
    Diesel_rate = models.DecimalField(max_digits=10, decimal_places=2)

class DailyNetSalesinCash(models.Model):
    Date = models.DateField(auto_now_add = True)
    Cash_from_Petrol_sales = models.DecimalField(max_digits=10, decimal_places=2)
    Cash_from_Diesel_sales = models.DecimalField(max_digits=10, decimal_places=2)

class Other_revenue(models.Model):
    Date = models.DateField(auto_now_add= True)
    Item = models.CharField(max_length = 50)
    qty = models.DecimalField(max_digits=10, decimal_places=2)
    rate = models.DecimalField(max_digits=10, decimal_places=2)
    Amount = models.DecimalField(max_digits=10, decimal_places=2,null = True)

    def calculate_total_amount(self):
        self.Amount = self.qty * self.rate
        super(Other_revenue,self).save()


class Cash_From_Previous_Credit(models.Model):
    Date = models.DateField(auto_now_add = True)
    VehicleNo = models.CharField(max_length = 100)
    Customer_name = models.CharField(max_length = 50)
    Customer_address = models.CharField(max_length = 255)
    RecieptNo = models.CharField(max_length = 100)
    Cash_Amount = models.DecimalField(max_digits=10, decimal_places=2)

class Closing_dip(models.Model):
    Date = models.DateField(auto_now_add= True)
    Petrol_16kl_closing_dip = models.DecimalField(max_digits = 10, decimal_places= 2)
    Petrol_16kl_closing_litres = models.DecimalField(max_digits = 10,decimal_places= 2)
    Diesel_16kl_closing_dip = models.DecimalField(max_digits = 10, decimal_places= 2)
    Diesel_16kl_closing_litres = models.DecimalField(max_digits = 10,decimal_places= 2)
    Diesel_22kl_closing_dip = models.DecimalField(max_digits = 10, decimal_places= 2)
    Diesel_22kl_closing_litres = models.DecimalField(max_digits = 10,decimal_places= 2)

class Dip_purchase(models.Model):
    Date = models.DateField(auto_now_add= True)
    Petrol_16kl_purchase = models.DecimalField(max_digits = 10, decimal_places= 4)
    Diesel_16kl_purchase = models.DecimalField(max_digits = 10, decimal_places= 4)
    Diesel_22kl_purchase = models.DecimalField(max_digits = 10, decimal_places= 4)

class Credit_details(models.Model):
    Date = models.DateField(auto_now_add = True)
    Customer_name = models.CharField(max_length = 50)
    Customer_address = models.CharField(max_length = 255)
    VehicleNo = models.CharField(max_length = 50)
    RecieptNo = models.CharField(max_length = 50)
    product = models.CharField(max_length = 25)
    litres = models.DecimalField(max_digits = 10,decimal_places= 2)
    rate = models.DecimalField(max_digits = 10 , decimal_places = 2)
    Credit_Amt = models.DecimalField(max_digits=10, decimal_places=2)


class Expenditure_details(models.Model):
    Date = models.DateField(auto_now_add = True)
    Details = models.CharField(max_length = 100)
    Amount = models.DecimalField(max_digits=10, decimal_places=2)

class Credit_Card_Payments(models.Model):
    Date = models.DateField(auto_now_add= True)
    Bank = models.CharField(max_length = 25)
    Amount = models.DecimalField(max_digits = 10 , decimal_places = 2)


class EOD_Total_balances_revenue(models.Model):
    Date = models.DateField(auto_now_add = True)
    Net_Petrol = models.DecimalField(max_digits=10, decimal_places=2, unique_for_date = Date)
    Net_Diesel = models.DecimalField(max_digits=10, decimal_places=2, unique_for_date = Date)

    Other_revenue = models.DecimalField(max_digits=10, decimal_places=2, unique_for_date = Date)
    Total_balances = models.DecimalField(max_digits=10, decimal_places=2,null = True, unique_for_date = Date)

    def calculate_total(self):
        self.Total_balances = self.Net_Petrol + self.Net_Diesel + self.Other_revenue
        super(EOD_Total_balances_revenue, self).save()

class EOD_Cash_account(models.Model):
    Date = models.DateField(auto_now_add = True)
    Closing_Cash_Balance = models.DecimalField(max_digits=10, decimal_places=2, unique_for_date = Date)
