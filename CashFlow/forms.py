from django.forms import ModelForm
from .models import *
from django.forms.models import modelformset_factory
from django.contrib.auth.models import User
from django import forms
from functools import partial

# DateInput = partial(forms.DateInput, {'class': 'datepicker'})

# class UserLogin(forms.Form):
#     username = forms.CharField(max_length = 15)
#     password = forms.CharField(widget = forms.PasswordInput)
#     class Meta:
#         model = User
#         fields = ('username','password',)

class SalesForm(ModelForm):
    class Meta:
        model = DailySales
        exclude =('Date','Gross_sales',)

class RatesForm(ModelForm):
    class Meta:
        model = DailyRates
        exclude = ('Date',)

class Cash_From_Previous_Credit_Sales(ModelForm):

    class Meta:
        model = Cash_From_Previous_Credit
        exclude = ('Date',)

class Other_revenue_form(ModelForm):
    class Meta:
        model = Other_revenue
        exclude = ('Date','Amount',)

class Credit_Card_form(ModelForm):
    class Meta:
        model = Credit_Card_Payments
        exclude = ('Date',)

class Credit_details_form(ModelForm):

    class Meta:
        model = Credit_details
        exclude =('Date',)

class Expenditure_form(ModelForm):
    class Meta:
        model = Expenditure_details
        exclude = ('Date',)

class Closing_dip_calculation(ModelForm):
    class Meta:
        model = Closing_dip
        exclude = ('Date','Petrol_16kl_closing_litres','Diesel_16kl_closing_litres','Diesel_22kl_closing_litres',)

class Dip_purchase_form(ModelForm):
    class Meta:
        model = Dip_purchase
        exclude = ('Date',)

class DateSummary(forms.Form):
    Date = forms.DateField(widget = forms.TextInput(attrs={'class':'datepicker'}))
