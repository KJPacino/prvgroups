# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0013_other_revenue_rate'),
    ]

    operations = [
        migrations.DeleteModel(
            name='loose_2t',
        ),
        migrations.RemoveField(
            model_name='eod_total_balances_revenue',
            name='Net_loose_2t',
        ),
    ]
