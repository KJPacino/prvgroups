# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('CashFlow', '0024_closing_dip_dip_purchase'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('access', models.CharField(max_length=1, choices=[('O', 'Owner'), ('A', 'Attendant'), ('M', 'Manager')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='eod_cash_account',
            name='Closing_Cash_Balance',
            field=models.DecimalField(max_digits=10, decimal_places=3, unique_for_date=models.DateField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Diesel',
            field=models.DecimalField(max_digits=10, decimal_places=3, unique_for_date=models.DateField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Petrol',
            field=models.DecimalField(max_digits=10, decimal_places=3, unique_for_date=models.DateField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Other_revenue',
            field=models.DecimalField(max_digits=10, decimal_places=3, unique_for_date=models.DateField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Total_balances',
            field=models.DecimalField(null=True, max_digits=10, decimal_places=3, unique_for_date=models.DateField(auto_now_add=True)),
        ),
    ]
