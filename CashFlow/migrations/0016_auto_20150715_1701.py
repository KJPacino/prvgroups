# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0015_auto_20150715_1230'),
    ]

    operations = [
        migrations.RenameField(
            model_name='credit_details',
            old_name='RecipetNo',
            new_name='RecieptNo',
        ),
        migrations.RenameField(
            model_name='credit_details',
            old_name='Vehicle',
            new_name='VehicleNo',
        ),
        migrations.AddField(
            model_name='cash_from_previous_credit',
            name='VehicleNo',
            field=models.CharField(default='it', max_length=100),
            preserve_default=False,
        ),
    ]
