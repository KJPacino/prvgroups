# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0020_auto_20150720_1019'),
    ]

    operations = [
        migrations.CreateModel(
            name='Credit_Card_Payments',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('Date', models.DateField(auto_now_add=True)),
                ('Bank', models.CharField(max_length=25)),
                ('Amount', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
        ),
    ]
