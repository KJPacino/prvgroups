# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailysales',
            name='Gross_sales',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='dailysales',
            name='Pump_name',
            field=models.CharField(choices=[('P1', 'MS-1'), ('P2', 'MS-2'), ('P3', 'MS-3'), ('P4', 'MS-4'), ('D1', 'HSD-1'), ('D2', 'HSD-2'), ('D3', 'HSD-3'), ('D4', 'HSD-4')], max_length=5),
        ),
    ]
