# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0014_auto_20150715_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='other_revenue',
            name='Amount',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='qty',
            field=models.IntegerField(),
        ),
    ]
