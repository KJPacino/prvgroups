# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0019_auto_20150717_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailysales',
            name='Pump_name',
            field=models.CharField(max_length=5, choices=[('MS-1', 'MS-1'), ('MS-2', 'MS-2'), ('MS-3', 'MS-3'), ('MS-4', 'MS-4'), ('HSD-1', 'HSD-1'), ('HSD-2', 'HSD-2'), ('HSD-3', 'HSD-3'), ('HSD-4', 'HSD-4'), ('HSD-5', 'HSD-5')], unique_for_date='Date'),
        ),
    ]
