# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0016_auto_20150715_1701'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailysales',
            name='Pump_name',
            field=models.CharField(unique_for_date='Date', max_length=5, choices=[('P1', 'MS-1'), ('P2', 'MS-2'), ('P3', 'MS-3'), ('P4', 'MS-4'), ('D1', 'HSD-1'), ('D2', 'HSD-2'), ('D3', 'HSD-3'), ('D4', 'HSD-4')]),
        ),
    ]
