# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0008_auto_20150714_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='expenditure_details',
            name='Amount',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='expenditure_details',
            name='Date',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2015, 7, 14, 13, 45, 42, 292630, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='expenditure_details',
            name='Details',
            field=models.CharField(max_length=100, default='hello'),
            preserve_default=False,
        ),
    ]
