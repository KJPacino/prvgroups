# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0012_cash_from_previous_credit'),
    ]

    operations = [
        migrations.AddField(
            model_name='other_revenue',
            name='rate',
            field=models.IntegerField(default=10),
            preserve_default=False,
        ),
    ]
