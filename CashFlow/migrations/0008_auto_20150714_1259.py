# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0007_loose_2t_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loose_2t',
            name='Amount',
            field=models.IntegerField(null=True),
        ),
    ]
