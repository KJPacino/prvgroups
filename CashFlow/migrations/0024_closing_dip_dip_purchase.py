# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0023_auto_20150722_1433'),
    ]

    operations = [
        migrations.CreateModel(
            name='Closing_dip',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('Date', models.DateField(auto_now_add=True)),
                ('Petrol_16kl_closing_dip', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Petrol_16kl_closing_litres', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_16kl_closing_dip', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_16kl_closing_litres', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_22kl_closing_dip', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_22kl_closing_litres', models.DecimalField(max_digits=10, decimal_places=4)),
            ],
        ),
        migrations.CreateModel(
            name='Dip_purchase',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('Date', models.DateField(auto_now_add=True)),
                ('Petrol_16kl_purchase', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_16kl_purchase', models.DecimalField(max_digits=10, decimal_places=4)),
                ('Diesel_22kl_purchase', models.DecimalField(max_digits=10, decimal_places=4)),
            ],
        ),
    ]
