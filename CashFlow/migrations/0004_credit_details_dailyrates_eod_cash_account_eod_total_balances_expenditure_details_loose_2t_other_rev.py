# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0003_auto_20150714_0851'),
    ]

    operations = [
        migrations.CreateModel(
            name='Credit_details',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('Date', models.DateField(auto_now_add=True)),
                ('Vehicle', models.CharField(max_length=50)),
                ('RecipetNo', models.CharField(max_length=50)),
                ('Credit_Amt', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='DailyRates',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('Date', models.DateField(auto_now_add=True)),
                ('Petrol_rate', models.IntegerField()),
                ('Diesel_rate', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='EOD_Cash_account',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='EOD_Total_balances',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Expenditure_details',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='loose_2t',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('Date', models.DateField(auto_now_add=True)),
                ('litres', models.IntegerField()),
                ('Rate', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Other_revenue',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('Date', models.DateField(auto_now_add=True)),
                ('Item', models.CharField(max_length=50)),
                ('qty', models.IntegerField(null=True)),
                ('Amount', models.IntegerField()),
            ],
        ),
    ]
