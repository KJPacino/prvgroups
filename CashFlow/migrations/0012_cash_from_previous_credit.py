# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0011_auto_20150715_1026'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cash_From_Previous_Credit',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('Date', models.DateField(auto_now_add=True)),
                ('RecieptNo', models.CharField(max_length=100)),
                ('Cash_Amount', models.IntegerField()),
            ],
        ),
    ]
