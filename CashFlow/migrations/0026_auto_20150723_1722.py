# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0025_auto_20150723_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eod_cash_account',
            name='Closing_Cash_Balance',
            field=models.DecimalField(decimal_places=3, max_digits=10, unique_for_date=models.DateTimeField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_cash_account',
            name='Date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Diesel',
            field=models.DecimalField(decimal_places=3, max_digits=10, unique_for_date=models.DateTimeField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Petrol',
            field=models.DecimalField(decimal_places=3, max_digits=10, unique_for_date=models.DateTimeField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Other_revenue',
            field=models.DecimalField(decimal_places=3, max_digits=10, unique_for_date=models.DateTimeField(auto_now_add=True)),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Total_balances',
            field=models.DecimalField(decimal_places=3, null=True, max_digits=10, unique_for_date=models.DateTimeField(auto_now_add=True)),
        ),
    ]
