# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0004_credit_details_dailyrates_eod_cash_account_eod_total_balances_expenditure_details_loose_2t_other_rev'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyNetSales',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('Date', models.DateField(auto_now_add=True)),
                ('Petrol_net_sales', models.IntegerField()),
                ('Diesel_net_sales', models.IntegerField()),
            ],
        ),
    ]
