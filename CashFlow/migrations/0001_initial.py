# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DailySales',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('Pump_name', models.CharField(unique_for_date=True, max_length=5, choices=[('P1', 'MS-1'), ('P2', 'MS-2'), ('P3', 'MS-3'), ('P4', 'MS-4'), ('D1', 'HSD-1'), ('D2', 'HSD-2'), ('D3', 'HSD-3'), ('D4', 'HSD-4')])),
                ('Opening_reading', models.IntegerField()),
                ('Closing_reading', models.IntegerField()),
                ('Gross_sales', models.IntegerField()),
                ('Date', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
