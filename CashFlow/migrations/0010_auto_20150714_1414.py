# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0009_auto_20150714_1345'),
    ]

    operations = [
        migrations.CreateModel(
            name='EOD_Total_balances_revenue',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('Date', models.DateField(auto_now_add=True)),
                ('Net_Petrol', models.IntegerField()),
                ('Net_Diesel', models.IntegerField()),
                ('Net_loose_2t', models.IntegerField()),
                ('Other_revenue', models.IntegerField()),
                ('Total_balances', models.IntegerField(null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='EOD_Total_balances',
        ),
    ]
