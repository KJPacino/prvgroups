# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0021_credit_card_payments'),
    ]

    operations = [
        migrations.AlterField(
            model_name='other_revenue',
            name='qty',
            field=models.DecimalField(decimal_places=3, max_digits=10),
        ),
    ]
