# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0027_auto_20150723_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='location',
            field=models.CharField(choices=[('P', 'Petrol Bunk'), ('T', 'Theater/Marriage Hall')], max_length=1, default='P'),
            preserve_default=False,
        ),
    ]
