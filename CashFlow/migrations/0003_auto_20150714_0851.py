# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0002_auto_20150714_0850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailysales',
            name='Gross_sales',
            field=models.IntegerField(null=True),
        ),
    ]
