# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0029_auto_20150726_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='credit_details',
            name='rate',
            field=models.DecimalField(default=0, decimal_places=2, max_digits=10),
            preserve_default=False,
        ),
    ]
