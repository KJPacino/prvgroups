# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0022_auto_20150721_1050'),
    ]

    operations = [
        migrations.AddField(
            model_name='cash_from_previous_credit',
            name='Customer_address',
            field=models.CharField(max_length=255, default='name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cash_from_previous_credit',
            name='Customer_name',
            field=models.CharField(max_length=50, default='name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='credit_details',
            name='Customer_address',
            field=models.CharField(max_length=255, default='address'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='credit_details',
            name='Customer_name',
            field=models.CharField(max_length=50, default='name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='credit_details',
            name='litres',
            field=models.DecimalField(decimal_places=3, max_digits=10, default=23),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='credit_details',
            name='product',
            field=models.CharField(max_length=25, default='petrol'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cash_from_previous_credit',
            name='Cash_Amount',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='credit_card_payments',
            name='Amount',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='credit_details',
            name='Credit_Amt',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='dailynetsalesincash',
            name='Cash_from_Diesel_sales',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='dailynetsalesincash',
            name='Cash_from_Petrol_sales',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='dailyrates',
            name='Diesel_rate',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='dailyrates',
            name='Petrol_rate',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='eod_cash_account',
            name='Closing_Cash_Balance',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Diesel',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Petrol',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Other_revenue',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Total_balances',
            field=models.DecimalField(max_digits=10, null=True, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='expenditure_details',
            name='Amount',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='Amount',
            field=models.DecimalField(max_digits=10, null=True, decimal_places=3),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='rate',
            field=models.DecimalField(max_digits=10, decimal_places=3),
        ),
    ]
