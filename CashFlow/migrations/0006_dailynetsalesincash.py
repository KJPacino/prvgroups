# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0005_dailynetsales'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyNetSalesinCash',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Date', models.DateField(auto_now_add=True)),
                ('Cash_from_Petrol_sales', models.IntegerField()),
                ('Cash_from_Diesel_sales', models.IntegerField()),
            ],
        ),
    ]
