# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0028_userprofile_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='user',
        ),
        migrations.AlterField(
            model_name='cash_from_previous_credit',
            name='Cash_Amount',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Diesel_16kl_closing_dip',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Diesel_16kl_closing_litres',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Diesel_22kl_closing_dip',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Diesel_22kl_closing_litres',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Petrol_16kl_closing_dip',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='closing_dip',
            name='Petrol_16kl_closing_litres',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='credit_card_payments',
            name='Amount',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='credit_details',
            name='Credit_Amt',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='credit_details',
            name='litres',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='dailynetsalesincash',
            name='Cash_from_Diesel_sales',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='dailynetsalesincash',
            name='Cash_from_Petrol_sales',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='dailyrates',
            name='Diesel_rate',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='dailyrates',
            name='Petrol_rate',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='eod_cash_account',
            name='Closing_Cash_Balance',
            field=models.DecimalField(unique_for_date=models.DateField(auto_now_add=True), decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Diesel',
            field=models.DecimalField(unique_for_date=models.DateField(auto_now_add=True), decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Net_Petrol',
            field=models.DecimalField(unique_for_date=models.DateField(auto_now_add=True), decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Other_revenue',
            field=models.DecimalField(unique_for_date=models.DateField(auto_now_add=True), decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='eod_total_balances_revenue',
            name='Total_balances',
            field=models.DecimalField(unique_for_date=models.DateField(auto_now_add=True), null=True, decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='expenditure_details',
            name='Amount',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='Amount',
            field=models.DecimalField(null=True, decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='qty',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.AlterField(
            model_name='other_revenue',
            name='rate',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
        migrations.DeleteModel(
            name='UserProfile',
        ),
    ]
