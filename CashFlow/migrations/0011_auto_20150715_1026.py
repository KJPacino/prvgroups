# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('CashFlow', '0010_auto_20150714_1414'),
    ]

    operations = [
        migrations.AddField(
            model_name='eod_cash_account',
            name='Closing_Cash_Balance',
            field=models.IntegerField(default=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eod_cash_account',
            name='Date',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2015, 7, 15, 10, 26, 6, 980308, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
