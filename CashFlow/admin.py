from django.contrib import admin
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from django.contrib.auth.admin import UserAdmin
from .models import *



class DailySale(admin.ModelAdmin):
    list_display = ('Pump_name','Opening_reading','Closing_reading','Gross_sales','Date',)

admin.site.register(DailySales,DailySale)

# class UserProfileInline(admin.StackedInline):
#
#     model = UserProfile
#
# class UserProfileAdmin(UserAdmin):
#     inlines = [UserProfileInline,]
#
# admin.site.unregister(User)
# admin.site.register(User,UserProfileAdmin)
# admin.site.register(User)
