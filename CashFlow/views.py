from django.shortcuts import render , redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
from datetime import date
from django.db.models import Sum
import xlrd
from django.contrib.auth import authenticate, login, logout
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from UserAccount.models import *

today = date.today()

Diesel_22KL = xlrd.open_workbook('/home/KjPacino/prvgroups/22KL-diesel.xlsx')
sh22D = Diesel_22KL.sheet_by_index(0)
lookup22D = dict(zip(sh22D.col_values(0, 1), sh22D.col_values(1, 1)))
Diesel_16KL = xlrd.open_workbook('/home/KjPacino/prvgroups/16Kl-diesel.xlsx')
sh16D = Diesel_16KL.sheet_by_index(0)
lookup16D = dict(zip(sh16D.col_values(0, 1), sh16D.col_values(1, 1)))
Petrol_16KL = xlrd.open_workbook('/home/KjPacino/prvgroups/16-kL-petrol.xlsx')
sh16P = Petrol_16KL.sheet_by_index(0)
lookup16P = dict(zip(sh16P.col_values(0, 1), sh16P.col_values(1, 1)))

def check_if_committed(): #function to check if something has already been comitted for that day.
    if EOD_Total_balances_revenue.objects.filter(Date = today).exists():
        return True
    else:
        return False

    return already_committed

commit = check_if_committed() #running above function

def index(request):
    return render(request,'index.html')

@login_required(login_url = 'login')
def Calculate_daily_sales(request):
    # if commit == True:
    #     table = DailySales.objects.all()
    # else:
    table = DailySales.objects.filter(Date = today)
    if len(table) == 9:
        message = 'Please enter the daily Rate'

    elif len(table) > 9 :
        message = 'There are more than 9 readings for the day'
    else:
        message = None
    if request.method == 'POST':
        sale_Form = SalesForm(data = request.POST)

        if sale_Form.is_valid():
            gross = sale_Form.save(commit=False)
            gross.Calc_gross()
            gross.save()
            return redirect('calculate_daily_sales')
    else:
        table = DailySales.objects.filter(Date = today)
        sale_Form = SalesForm()
    return render(request,'SalesForm.html',{'SalesForm':sale_Form, 'table':table, 'message':message,
    'already_committed':commit})

@login_required(login_url = 'login')
def calculate_closing_dip(request):
    table =  Closing_dip.objects.filter(Date = today)
    if request.method== 'POST':
        dip_chart = Closing_dip_calculation(request.POST)
        if dip_chart.is_valid():
            KL22_Diesel_Dip = float(dip_chart.cleaned_data['Diesel_22kl_closing_dip'])
            KL16_Petrol_Dip = float(dip_chart.cleaned_data['Petrol_16kl_closing_dip'])
            KL16_Diesel_Dip = float(dip_chart.cleaned_data['Diesel_16kl_closing_dip'])
            KL22_Diesel_Liters = lookup22D[KL22_Diesel_Dip]
            KL16_Petrol_Liters = lookup16P[KL16_Petrol_Dip]
            KL16_Diesel_Liters = lookup16D[KL16_Diesel_Dip]

            record = Closing_dip(Petrol_16kl_closing_dip = KL16_Petrol_Dip,
            Petrol_16kl_closing_litres = KL16_Petrol_Liters,
            Diesel_16kl_closing_dip = KL16_Diesel_Dip,
            Diesel_16kl_closing_litres = KL16_Diesel_Liters,
            Diesel_22kl_closing_dip = KL22_Diesel_Dip,
            Diesel_22kl_closing_litres = KL22_Diesel_Liters )

            record.save()

            return render(request, 'Dip_calc.html',{'dip':dip_chart,
            'table':table})

    dip_chart = Closing_dip_calculation()

    return render(request,'Dip_calc.html',{'dip':dip_chart, 'already_committed':commit,
    'table':table})


def calculate_net_from_gross():
    Net_Petrol_Sales = 0
    Net_Diesel_Sales = 0

    try:
        Petrol_sales = DailySales.objects.filter(Date = today, Pump_name__startswith = 'M')
    except:
        Petrol_sales = None

    if Petrol_sales:
        for Psales in Petrol_sales:
            Net_Petrol_Sales += Psales.Gross_sales
    else:
        Net_Petrol_Sales = 0

    try:
        Diesel_sales = DailySales.objects.filter(Date = today, Pump_name__startswith = 'H')
    except:
        Diesel_sales = None

    if Diesel_sales:
        for Dsales in Diesel_sales:
            Net_Diesel_Sales +=Dsales.Gross_sales
    else:
        Net_Diesel_Sales = 0

    if Net_Petrol_Sales and Net_Diesel_Sales:
        record = DailyNetSales(Petrol_net_sales = Net_Petrol_Sales, Diesel_net_sales = Net_Diesel_Sales)
        record.save()
    else:
        return HttpResponse('Somethings wrong with calculate_net')

@login_required(login_url = 'login')
def Calculate_rates(request):
    table = DailyRates.objects.filter(Date = today)
    Net_table =  DailyNetSalesinCash.objects.filter(Date = today)

    if request.method == 'POST':
        calculate_net_from_gross()
        rates_form = RatesForm(data = request.POST)
        petrol_rate = float(request.POST['Petrol_rate'])
        diesel_rate = float(request.POST['Diesel_rate'])

        try:
            todays_sales = DailyNetSales.objects.filter(Date=today)
        except:
            redirect('calculate_daily_sales')

        for todays_sale in todays_sales:
            Cash_from_petrol_sales = todays_sale.Petrol_net_sales * petrol_rate
            Cash_from_diesel_sales = todays_sale.Diesel_net_sales * diesel_rate

        total_cash = DailyNetSalesinCash(Cash_from_Petrol_sales = Cash_from_petrol_sales,Cash_from_Diesel_sales = Cash_from_diesel_sales)
        total_cash.save()

        todays_rates = DailyRates(Petrol_rate = petrol_rate,Diesel_rate = diesel_rate)

        todays_rates.save()

        return redirect('calculate_rates')
    else:

        rates_form = RatesForm()

        return render(request,'Rates.html',{'Rates_form':rates_form,
        'table':table, 'already_committed':commit, 'Net_table':Net_table})

@login_required(login_url = 'login')
def DipPurchase(request):
    table = Dip_purchase.objects.filter(Date = today)

    if request.method == 'POST':
        dip_purchase = Dip_purchase_form(request.POST)

        if dip_purchase.is_valid():
            dip_purchase.save()
            P16KL = dip_purchase.cleaned_data['Petrol_16kl_purchase']
            D16KL = dip_purchase.cleaned_data['Diesel_16kl_purchase']
            D22KL = dip_purchase.cleaned_data['Diesel_22kl_purchase']
            return redirect('dip-purchase')
    else:
        dip_purchase = Dip_purchase_form()

        return render(request,'dip-purchase.html',{'dip_purchase':dip_purchase,'already_committed':commit,
        'table':table})

@login_required(login_url = 'login')
def Calculate_Other_Revenue(request):
    table = Other_revenue.objects.filter(Date = today)
    total_revenue = 0
    for i in table:
        total_revenue += i.Amount

    if request.method == 'POST':
        other_revenue = Other_revenue_form(request.POST)
        if other_revenue.is_valid():
            other = other_revenue.save(commit=False)
            other.calculate_total_amount()
            other.save()
            return redirect('other_revenue')
    else:
        other_revenue = Other_revenue_form()
        return render(request,'other-revenue.html',{'other_revenue':other_revenue, 'table':table, 'total_revenue':total_revenue,
        'already_committed':commit})

@login_required(login_url = 'login')
def Calculate_Cash_From_Credit(request):
    table = Cash_From_Previous_Credit.objects.filter(Date = today)
    total_credit = 0
    for i in table:
        total_credit += i.Cash_Amount

    if request.method == 'POST':

        cash_from_credit = Cash_From_Previous_Credit_Sales(request.POST)

        if cash_from_credit.is_valid():
            cash = cash_from_credit.save()
            return redirect('cash_from_credit')

    else:
        cash_from_credit = Cash_From_Previous_Credit_Sales()

        return render(request,'Cash-from-credit.html',{'Cash_Credit':cash_from_credit,'table':table, 'total_credit':total_credit,
        'already_committed':commit})

@login_required(login_url = 'login')
def Credit_details_list(request):
    table = Credit_details.objects.filter(Date = today)
    total_credit = 0
    for i in table:
        total_credit += i.Credit_Amt
    if request.method == 'POST':
        credit = Credit_details_form(request.POST)

        if credit.is_valid():
            credit.save()
        return redirect('Credit')
    else:
        credit = Credit_details_form()

        return render(request,'Credit-details.html',{'Credit':credit,'table':table,'total_credit':total_credit,
        'already_committed':commit})

@login_required(login_url = 'login')
def Expenditure(request):
    table = Expenditure_details.objects.filter(Date = today)
    total_expenses = 0
    for i in table:
        total_expenses += i.Amount
    if request.method == 'POST':
        exp = Expenditure_form(request.POST)

        if exp.is_valid():
            exp.save()
        return redirect('Expenditure')

    else:
        exp = Expenditure_form()

        return render(request,'Expenditure.html',{'expense':exp,
        'table':table,
        'total_expenses':total_expenses,
        'already_committed':commit})

@login_required(login_url = 'login')
def Credit_Card_Payment(request):
    table = Credit_Card_Payments.objects.filter(Date = today)
    card_payments = 0
    for i in table:
        card_payments += i.Amount
    if request.method == 'POST':
        card = Credit_Card_form(request.POST)

        if card.is_valid():
            card.save()
        return redirect('Credit_Card')
    else:
        card = Credit_Card_form()

        return render(request,'Card.html',{'table':table,'card':card,
        'card_payments':card_payments,
        'already_committed':commit})

@login_required(login_url = 'login')
def Calculate(request):
    #updating date field everytime a user comes to main page
    global today
    today = date.today()
    global commit
    commit = check_if_committed()


    u = UserProfile.objects.get(user = request.user)
    if u.access == 'M':
        manager_access = True
    else:
        manager_access = False

    try:
        dailysales = DailySales.objects.filter(Date = today)
    except:
        return HttpResponse('Please enter petrol sales')

    try:
        Petrol_sales = DailySales.objects.filter(Date = today, Pump_name__startswith = 'M')
    except:
        return HttpResponse('No Petrol Sales Found')

    try:
        Diesel_sales = DailySales.objects.filter(Date = today, Pump_name__startswith = 'H')
    except:
        return HttpResponse('No Diesel Sales Found')

    try:
        totalpumpsales = DailyNetSalesinCash.objects.filter(Date = today)
    except:
        return HttpResponse('Please enter petrol/Diesel rates')

    try:
        other_rev = Other_revenue.objects.filter(Date = today)
    except:
        return HttpResponse('Please enter other revenue streams')

    try:
        cash_from_credit_sales = Cash_From_Previous_Credit.objects.filter(Date=today)
    except:
        return HttpResponse('Please enter your cash from credit')

    try:
        Credit_sales = Credit_details.objects.filter(Date = today)
    except:
        return HttpResponse('No Credit mentioned')

    try:
        Expenses = Expenditure_details.objects.filter(Date = today)
    except:
        return HttpResponse('No Expenses?')

    try:
        credit_card = Credit_Card_Payments.objects.filter(Date = today)
    except:
        return HttpResponse('No credit payments')

    try:
        dip_convertor = Closing_dip.objects.filter(Date = today)
    except:

        return HttpResponse('No Dip provided')

    try:
        Dip_Purchaser = Dip_purchase.objects.filter(Date = today)
    except:

        return HttpResponse('No Dip purchased')


    if EOD_Total_balances_revenue.objects.filter(Date=today).exists() and EOD_Cash_account.objects.filter(Date = today).exists():
        already_committed = True
    else:
        already_committed = False



    Netsales = DailyNetSales.objects.filter(Date = today)

    for i in Netsales:
        petrol_net = i.Petrol_net_sales
        diesel_net = i.Diesel_net_sales

    totalsales = 0
    if totalpumpsales:
        for i in totalpumpsales:
            EODpetrol = i.Cash_from_Petrol_sales
            totalsales += i.Cash_from_Petrol_sales
    else:
        EODpetrol = 0


    if totalpumpsales:
        for j in totalpumpsales:
            EODDiesel = i.Cash_from_Diesel_sales
            totalsales += j.Cash_from_Diesel_sales
    else:
        EODDiesel = 0

    sum_other = other_rev.aggregate(Sum('Amount')) #returns dict object
    cash_credit = cash_from_credit_sales.aggregate(Sum('Cash_Amount'))

    if sum_other['Amount__sum']:
        cash_other = sum_other['Amount__sum']
    else:
        cash_other = 0

    if cash_credit['Cash_Amount__sum']:
        cash_credit_sum = cash_credit['Cash_Amount__sum']
    else:
        cash_credit_sum = 0
    totalsales +=  cash_other + cash_credit_sum

    total_Credit_sales = 0
    for i in Credit_sales:
        total_Credit_sales += i.Credit_Amt


    total_expenditure = 0
    for i in Expenses:
        total_expenditure += i.Amount

    total_credit_card_payments = 0
    for i in credit_card:
        total_credit_card_payments += i.Amount

    Cash_Balance = totalsales - (total_Credit_sales + total_expenditure + total_credit_card_payments)


    return render(request,'total2.html',{'Total':totalsales,'Tot_credit': total_Credit_sales,
     'expenses':total_expenditure, 'Cash_Balance' :Cash_Balance,
     'netpetrol':EODpetrol,
     'netdiesel':EODDiesel,
     'dailysales':dailysales,
     'other_rev':other_rev,
     'cash_other':cash_other,
     'cash_credit':cash_credit_sum,
     'total_credit_card':total_credit_card_payments,
     'cash_from_credit_sales':cash_from_credit_sales,
     'Credit_sales':Credit_sales,
     'Expenses':Expenses,
     'Petrol_sales': Petrol_sales,
     'Diesel_sales': Diesel_sales,
     'credit_card': credit_card,
     'dip_convertor':dip_convertor,
     'manager':manager_access,
     'already_committed':already_committed,
     'Dip_Purchaser':Dip_Purchaser,
     })

@login_required(login_url = 'login')
def Commit_all(request):

    dailysales = DailySales.objects.filter(Date = today)
    totalpumpsales = DailyNetSalesinCash.objects.filter(Date = today)
    other_rev = Other_revenue.objects.filter(Date = today)
    cash_from_credit_sales = Cash_From_Previous_Credit.objects.filter(Date=today)
    Credit_sales = Credit_details.objects.filter(Date = today)
    Expenses = Expenditure_details.objects.filter(Date = today)
    credit_card = Credit_Card_Payments.objects.filter(Date = today)

    time = datetime.now()

    totalsales = 0

    for i in totalpumpsales:
        EODpetrol = i.Cash_from_Petrol_sales
        totalsales += i.Cash_from_Petrol_sales

    for j in totalpumpsales:
        EODDiesel = i.Cash_from_Diesel_sales
        totalsales += j.Cash_from_Diesel_sales

    total_credit_card_payments = 0
    for i in credit_card:
        total_credit_card_payments += i.Amount


    sum_other = other_rev.aggregate(Sum('Amount')) #returns dict object
    cash_credit = cash_from_credit_sales.aggregate(Sum('Cash_Amount'))

    if sum_other['Amount__sum']:
        cash_other = sum_other['Amount__sum']
    else:
        cash_other = 0

    if cash_credit['Cash_Amount__sum']:
        cash_credit_sum = cash_credit['Cash_Amount__sum']
    else:
        cash_credit_sum = 0
    totalsales +=  cash_other + cash_credit_sum

    create_record = EOD_Total_balances_revenue(Net_Petrol = EODpetrol,
    Net_Diesel = EODDiesel,
    Other_revenue = cash_other,
    Total_balances = totalsales,
    )

    total_Credit_sales = 0
    for i in Credit_sales:
        total_Credit_sales += i.Credit_Amt


    total_expenditure = 0
    for i in Expenses:
        total_expenditure += i.Amount

    create_record.save()

    Cash_Balance = totalsales - (total_Credit_sales + total_expenditure + total_credit_card_payments)


    closing_cash = EOD_Cash_account(Closing_Cash_Balance = Cash_Balance)
    closing_cash.save()

    return render(request, 'Commit.html', {'time':time})



@login_required(login_url = 'login')
def logout_view(request):
    logout(request)
    return redirect('login')

@login_required(login_url = 'login')
def delete_item_other(request,pk):
    key = Other_revenue.objects.filter(pk=pk)
    key.delete()
    return redirect('other_revenue')

@login_required(login_url = 'login')
def delete_Cash_from_Credit(request,pk):
    key = Cash_From_Previous_Credit.objects.filter(pk=pk)
    key.delete()
    return redirect('cash_from_credit')

@login_required(login_url = 'login')
def delete_Credit_detail(request,pk):
    key = Credit_details.objects.filter(pk=pk)
    key.delete()
    return redirect('Credit')

@login_required(login_url = 'login')
def delete_Closing_dip(request,pk):
    key = Closing_dip.objects.filter(pk=pk)
    key.delete()
    return redirect('closing-dip')

@login_required(login_url = 'login')
def delete_purchase_dip(request,pk):
    key = Dip_purchase.objects.filter(pk=pk)
    key.delete()
    return redirect('dip-purchase')

@login_required(login_url = 'login')
def delete_expenses(request,pk):
    key = Expenditure_details.objects.filter(pk=pk)
    key.delete()
    return redirect('Expenditure')

@login_required(login_url = 'login')
def delete_credit_card(request,pk):
    key = Credit_Card_Payments.objects.get(pk = pk)
    key.delete()
    return redirect('Credit_Card')

@login_required(login_url = 'login')
def delete_pump(request,pk):
    key = DailySales.objects.filter(pk=pk)
    key.delete()
    return redirect('calculate_daily_sales')

@login_required(login_url = 'login')
def delete_rates(request,pk):
    key = DailyRates.objects.filter(pk=pk)
    net_key = DailyNetSalesinCash.objects.filter(Date=today)
    net_sale = DailyNetSales.objects.filter(Date = today)
    net_sale.delete()
    key.delete()
    net_key.delete()
    return redirect('calculate_daily_sales')


#summary view common to both owner and attendant - need to check if i can transfer to common view file
def summaryview(request):

    if request.method == 'POST':
        datesum = DateSummary(request.POST)
        if datesum.is_valid():
            specified_date = datesum.cleaned_data['Date']

            # try:
            #     revenue_balance =  EOD_Total_balances_revenue.objects.filter(Date = specified_date)
            # except:
            #     revenue_balance = None

            # try:
            #     eod_cash = EOD_Cash_account.objects.filter(Date = specified_date)
            # except:
            #     eod_cash = None

            # try:
            #     expenses = Expenditure_details.objects.filter(Date = specified_date)
            #     total_expenses = 0
            #     for i in expenses:
            #         total_expenses += i.Amount
            # except:
            #     expenses = None
            #     total_expenses = 0

            # try:
            #     credit = Credit_details.objects.filter(Date = specified_date)
            #     tot_credit = 0
            #     for i in tot_credit:
            #         tot_credit += i.Credit_Amt
            # except:
            #     credit = None
            #     tot_credit = 0

            # try:
            #     closedip = Closing_dip.objects.filter(Date = specified_date)
            # except:
            #     closedip = None

            # try:
            #     purdip = Dip_purchase.objects.filter(Date = specified_date)
            # except:
            #     purdip = None

            # try:
            #     cashcredit = Cash_From_Previous_Credit.objects.filter(Date = specified_date)
            #     tot_cash = 0
            #     for i in cashcredit:
            #         tot_cash += i.Cash_Amount
            # except:
            #     cashcredit = None
            #     tot_cash = 0

            # return render(request,'archives.html',{'date':specified_date,
            # 'revenue_balance':revenue_balance,
            # 'eod_cash':eod_cash,
            # 'total_expenses':total_expenses,
            # 'tot_credit':tot_credit,
            # 'closedip':closedip,
            # 'purdip':purdip,
            # 'tot_cash': tot_cash})

            try:
                dailysales = DailySales.objects.filter(Date = specified_date)
            except:
                return HttpResponse('Please enter petrol sales')

            try:
                Petrol_sales = DailySales.objects.filter(Date = specified_date, Pump_name__startswith = 'M')
            except:
                return HttpResponse('No Petrol Sales Found')

            try:
                Diesel_sales = DailySales.objects.filter(Date = specified_date, Pump_name__startswith = 'H')
            except:
                return HttpResponse('No Diesel Sales Found')

            try:
                totalpumpsales = DailyNetSalesinCash.objects.filter(Date = specified_date)
            except:
                return HttpResponse('Please enter petrol/Diesel rates')

            try:
                other_rev = Other_revenue.objects.filter(Date = specified_date)
            except:
                return HttpResponse('Please enter other revenue streams')

            try:
                cash_from_credit_sales = Cash_From_Previous_Credit.objects.filter(Date=specified_date)
            except:
                return HttpResponse('Please enter your cash from credit')

            try:
                Credit_sales = Credit_details.objects.filter(Date = specified_date)
            except:
                return HttpResponse('No Credit mentioned')

            try:
                Expenses = Expenditure_details.objects.filter(Date = specified_date)
            except:
                return HttpResponse('No Expenses?')

            try:
                credit_card = Credit_Card_Payments.objects.filter(Date = specified_date)
            except:
                return HttpResponse('No credit payments')

            try:
                dip_convertor = Closing_dip.objects.filter(Date = specified_date)
            except:

                return HttpResponse('No Dip provided')

            try:
                Dip_Purchaser = Dip_purchase.objects.filter(Date = specified_date)
            except:

                return HttpResponse('No Dip purchased')


            if EOD_Total_balances_revenue.objects.filter(Date=specified_date).exists() and EOD_Cash_account.objects.filter(Date = today).exists():
                already_committed = True
            else:
                already_committed = False



            Netsales = DailyNetSales.objects.filter(Date = specified_date)

            for i in Netsales:
                petrol_net = i.Petrol_net_sales
                diesel_net = i.Diesel_net_sales

            totalsales = 0
            if totalpumpsales:
                for i in totalpumpsales:
                    EODpetrol = i.Cash_from_Petrol_sales
                    totalsales += i.Cash_from_Petrol_sales
            else:
                EODpetrol = 0


            if totalpumpsales:
                for j in totalpumpsales:
                    EODDiesel = i.Cash_from_Diesel_sales
                    totalsales += j.Cash_from_Diesel_sales
            else:
                EODDiesel = 0

            sum_other = other_rev.aggregate(Sum('Amount')) #returns dict object
            cash_credit = cash_from_credit_sales.aggregate(Sum('Cash_Amount'))

            if sum_other['Amount__sum']:
                cash_other = sum_other['Amount__sum']
            else:
                cash_other = 0

            if cash_credit['Cash_Amount__sum']:
                cash_credit_sum = cash_credit['Cash_Amount__sum']
            else:
                cash_credit_sum = 0
            totalsales +=  cash_other + cash_credit_sum

            total_Credit_sales = 0
            for i in Credit_sales:
                total_Credit_sales += i.Credit_Amt


            total_expenditure = 0
            for i in Expenses:
                total_expenditure += i.Amount

            total_credit_card_payments = 0
            for i in credit_card:
                total_credit_card_payments += i.Amount

            Cash_Balance = totalsales - (total_Credit_sales + total_expenditure + total_credit_card_payments)


            return render(request,'archives1.html',{'Total':totalsales,'Tot_credit': total_Credit_sales,
             'expenses':total_expenditure, 'Cash_Balance' :Cash_Balance,
             'netpetrol':EODpetrol,
             'netdiesel':EODDiesel,
             'dailysales':dailysales,
             'other_rev':other_rev,
             'cash_other':cash_other,
             'cash_credit':cash_credit_sum,
             'total_credit_card':total_credit_card_payments,
             'cash_from_credit_sales':cash_from_credit_sales,
             'Credit_sales':Credit_sales,
             'Expenses':Expenses,
             'Petrol_sales': Petrol_sales,
             'Diesel_sales': Diesel_sales,
             'credit_card': credit_card,
             'dip_convertor':dip_convertor,
             'Dip_Purchaser':Dip_Purchaser,
             })


    else:
        datesum = DateSummary()
        return render(request,'archives1.html',{'datesum':datesum})
